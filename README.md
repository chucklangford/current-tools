## Terminal

- Emulator - https://github.com/kovidgoyal/kitty
- Shell - https://www.zsh.org/
- Search - https://github.com/BurntSushi/ripgrep
- Search - https://www.gnu.org/software/grep/
- Search - https://github.com/ggreer/the_silver_searcher
- Downloading/API Testing - https://curl.se/
- Neovim - https://neovim.io/
- Vim (general purpose) - https://www.vim.org/
- Emacs (lisp) - https://www.gnu.org/software/emacs/
- Tmux - https://github.com/tmux/tmux
- Tmuxinator - https://github.com/tmuxinator/tmuxinator

## Networking
- Domain Registrar - https://domains.google.com
- VPN - https://www.tailscale.com/

- Local Service Hosting 
  - Free: https://localtunnel.github.io/www/ 
  - Paid: https://ngrok.com/ 

## Databases
- Free Client - https://dbeaver.io/
- Postgres - https://www.pgcli.com/
- SQLite - https://litecli.com/

## Files
- SyncThing - https://syncthing.net/

## Containers
- Docker - https://www.docker.com/
- https://containerd.io/

## Web Server
- https://nginx.org/en/

## Tools To Audition
- https://blog.jupyter.org/an-sql-solution-for-jupyter-ef4a00a0d925
- https://www.qemu.org/
- https://www.linux-kvm.org/page/Main_Page
- https://github.com/sindresorhus/awesome#development-environment
